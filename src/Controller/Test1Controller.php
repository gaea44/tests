<?php

namespace App\Controller;

use App\Entity\Test1;
use App\Entity\Test2;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class Test1Controller extends AbstractController
{
    /**
     * @Route("/test1", name="test1")
     */
    public function index()
    {
        $test2 = $this->getDoctrine()->getRepository(Test2::class)->findOneBy(['name' => 'test']);

        $test1 = new Test1();
        $test1->setTest2($test2);

        return $this->render('test1/index.html.twig', [
            'controller_name' => 'Test1Controller',
        ]);
    }
}
