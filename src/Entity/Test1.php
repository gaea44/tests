<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Test1Repository")
 */
class Test1
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var Test2
     *
     * @ORM\ManyToOne(targetEntity=Test2")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $test2;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Test1
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Test2|null
     */
    public function getTest2(): ?Test2
    {
        return $this->test2;
    }

    /**
     * @param Test2 $test2
     *
     * @return Test1
     */
    public function setTest2(Test2 $test2): self
    {
        $this->test2 = $test2;

        return $this;
    }
}
